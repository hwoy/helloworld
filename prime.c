#include <stdio.h>

static int isprime(unsigned int n)
{
	if(n<2)
		return 0;

	for(unsigned int i=2;i*i<=n;++i)
	{
		if(!(n%i))
			return 0;
	}

	return 1;
}

int main()
{
	unsigned int n=0;

	for(unsigned int i=1;i<=10000000;++i)
	{
		if(isprime(i))
			n=i;
	}

	printf("%u\n",n);

	return 0;
}
