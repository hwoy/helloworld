def iseven(n):
    if n%2==0:
        return True
    return False

def main():
    for n in filter(lambda x: iseven(x),range(1,100)):
        print(n)

if __name__ == "__main__":
    main()

