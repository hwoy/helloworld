fn isprime(n:u32)->bool
{
    if n<2
    {
        return false;
    }

    (2u32..).take_while(|&x| x*x<=n).all(|x| n%x != 0)
}


fn main()
{
   let mut n:u32=0;
   for i in (1..=10000000u32).filter(|&x| isprime(x))
   {
       n=i;
   }

   println!("{}",n);
}
