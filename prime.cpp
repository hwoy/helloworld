#include <iostream>

static bool isprime(unsigned int n)
{
	if(n<2)
		return false;

	for(unsigned int i=2;i*i<=n;++i)
	{
		if(!(n%i))
			return false;
	}

	return true;
}

int main()
{
	unsigned int n=0;

	for(unsigned int i=1;i<=10000000;++i)
	{
		if(isprime(i))
			n=i;
	}

	std::cout << n << '\n';

	return 0;
}
