def isprime(n):
    if n<2:
        return False
    i=2
    while i*i<=n:
        if n%i==0:
            return False
        i=i+1
    return True

def main():
    n=0
    for i in filter(lambda x: isprime(x),range(1,10000001)):
        n=i
    print(i)

if __name__ == "__main__":
    main()

